# Python Week Of Code, Namibia 2019

Django allow us to create form based in our models.

## Task for Instructor

1. Create the file [blog/forms.py](blog/forms.py) and add

   ```
   from django.forms import ModelForm

   from .models import Post

   class PostForm(ModelForm):
        class Meta:
            model = Post
            fields = [
                'title',
                'text',
            ]
   ```
2. Change function `blog_form` in [blog/views.py](blog/views.py) to

   ```
   from .forms import PostForm

   def blog_form(request):
       form = PostForm(
           request.POST if request.POST else None
    )
       if request.POST:
           if form.is_valid():
               post = form.save()
               return redirect('blog', title=post.title)

       return render(
           request,
           'blog/post_form.html',
           {
               "form": form,
           }
       )
   ```
3. Edit [blog/templates/blog/post_form.html](blog/templates/blog/post_form.html) to

   ```
   <!DOCTYPE html>
   <html>
   <head>
   <title>My Site - Blog - New Post</title>
   </head>
   <body>
   <article>
   <h1>New Post</h1>

   <form action="." method="post">
       {% csrf_token %}
       {{ form }
       <input type="submit" value="Submit">
   <form></form>
   </article>
   </body>
   </html>
   ```
4. Open http://localhost:8000/blog/ with your web browser. Fill the form and click on "Submit".

## Tasks for Learners

1. Add a field in the form to save the published date.
2. Change the view to save the published date.